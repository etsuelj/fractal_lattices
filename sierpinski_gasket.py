from __future__ import division
import numpy as np
import matplotlib.pyplot as plt

def midpoint(a,b):
	'''
	Calculate the midpoint between points a and b.
	
	Parameters
	----------
	a,b : array_like
		Contains the x and y coordinates of the two points a and b
	
	Returns
	-------
	out : ndarray
		Coordinates of the midpoint
	'''
	x1,y1,x2,y2 = a[0],a[1],b[0],b[1]
	x,y = (x1+x2)/2,(y1+y2)/2
	return [x,y]
	
def subtriangle(itera,corners,kulay):
	'''
	Find the midpoint of a triangle and draws three subtriangles.

	Parameters
	----------
	itera : int
		The nth iteration
	corners : array_like
		An array of length 3 with each element a tuple containing x and y coordinates of the corners
	kulay : string or tuple
		Color of the triangles
	'''	
	if itera < N:
		itera+=1		
		for i in xrange(len(corners)):			
			new_corners = [corners[i]]
			for j in xrange(len(corners)):
				if i!=j:
					new_corners.append(midpoint(corners[i],corners[j]))
					segment = zip(corners[i], midpoint(corners[i],corners[j]))
					plt.plot(segment[0],segment[1],color=kulay)
			subtriangle(itera,new_corners,kulay)

corner_init = np.array([[0.,0.],[0.5,-1.],[1.,0.]]) #corners of initial triangle
N = 5 #number of iterations

plt.plot(corner_init[:,0],corner_init[:,1],'r.') #plot corners of initial triangle
plt.axis('off') #remove axis
subtriangle(0,corner_init,'red')
plt.show()